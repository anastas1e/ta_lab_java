package com.epam.lab.lambdas;

import java.util.stream.IntStream;

public class MathLambda {
    @FunctionalInterface
    public interface Calculation {
        int calculate(int a, int b, int c);
    }

    public static void main(String[] args) {
        Calculation max = (a,b,c) -> IntStream.of(a, b, c).max().getAsInt();
        Calculation avg = (a,b,c) -> Math.round((float) IntStream.of(a, b, c).average().getAsDouble());
        System.out.println("Max = " + max.calculate(1, 2, 3));
        System.out.println("Average = " + avg.calculate(1, 2, 3));
    }
}
