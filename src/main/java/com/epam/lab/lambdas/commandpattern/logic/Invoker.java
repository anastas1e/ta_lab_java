package com.epam.lab.lambdas.commandpattern.logic;

public class Invoker {
    private Command printCommand;
    public Invoker(Command printCommand) {
        this.printCommand = printCommand;
    }
    public void print(String arg) {
        printCommand.execute(arg);
    }
}
