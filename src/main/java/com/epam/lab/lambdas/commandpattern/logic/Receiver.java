package com.epam.lab.lambdas.commandpattern.logic;

public class Receiver {
    public void print(String arg) {
        System.out.println(arg);
    }
}
