package com.epam.lab.lambdas.commandpattern.logic;

@FunctionalInterface
public interface Command {
    void execute(String arg);
}
