package com.epam.lab.lambdas.commandpattern.logic;

public class CommandReceiver implements Command {
    private Receiver rec;
    public CommandReceiver(Receiver rec) {
        this.rec = rec;
    }
    @Override
    public void execute(String arg) {
        rec.print(arg);
    }
}
