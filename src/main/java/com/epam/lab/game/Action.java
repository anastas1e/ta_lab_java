package com.epam.lab.game;

public interface Action {
    Choice getChoice();
}
