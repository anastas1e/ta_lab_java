package com.epam.lab.game;
import java.util.Random;

public class Computer implements Action {
    @Override
    public Choice getChoice() {
        Choice[] choices = Choice.values();
        Random random = new Random();
        return choices[random.nextInt(choices.length)];
    }
}