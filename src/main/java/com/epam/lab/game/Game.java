package com.epam.lab.game;

public class Game {

    private Computer computer;
    private Player player;

    public Game() {
        this.computer = new Computer();
        this.player = new Player();
    }

    public void start() {
        System.out.println("Game started!");
        Choice playerChoice = player.getChoice();
        Choice cpuChoice = null;
        cpuChoice = computer.getChoice();
        System.out.printf("Your choice : %s%n", playerChoice);
        System.out.printf("Computer choice : %s%n", cpuChoice);
        showWinner(playerChoice, cpuChoice);

        if (player.playAgain()) {
            System.out.println();
            start();
        } else {
            System.out.println("end of the game!");
        }
    }

    public void showWinner(Choice playerChoice, Choice cpuChoice) {
        int result = playerChoice.getResult(cpuChoice);
        if (result == 0) {
            System.out.println("Tie!");
        } else if (result == 1) {
            System.out.println("Player wins!");
        } else if (result == -1) {
            System.out.println("Computer wins!");
        }
    }

    public static void main(String[] args) {
        Game game = new Game();
        game.start();
    }
}
