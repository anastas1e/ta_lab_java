package com.epam.lab.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class RectangleArea {
    private static int getRectangleArea(int a, int b) {
        if (a <= 0 || b <= 0) {
            throw new IllegalArgumentException("Value of the sides can not be negative!");
        }
        return a * b;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Please, enter the first side of the rectangle: ");
            int a = scanner.nextInt();
            System.out.println("Please, enter the second side of the rectangle: ");
            int b = scanner.nextInt();
            System.out.println("The area of given rectangle is " + getRectangleArea(a, b));
        } catch (InputMismatchException e) {
            System.out.println("You entered non-numeric value!");
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
 }

