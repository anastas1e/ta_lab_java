package com.epam.lab.exceptions.plants;

//KISS principle used
public enum Type {
    TREE, FLOWER, BUSH, MOSS, ALGAE;

    public String toString() {
        String s = "";
        switch (this) {
            case TREE: s = "tree"; break;
            case FLOWER: s = "flower"; break;
            case BUSH: s = "bush"; break;
            case MOSS: s = "moss"; break;
            case ALGAE: s = "algae"; break;
        }
        return s;
    }
}
