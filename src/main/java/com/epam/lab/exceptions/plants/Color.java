package com.epam.lab.exceptions.plants;

//KISS principle used
public enum Color {
    GREEN, YELLOW, BLUE, PINK, RED, ORANGE;

    public String toString() {
        String s = "";
        switch (this) {
            case GREEN: s = "green"; break;
            case YELLOW: s = "yellow"; break;
            case BLUE: s = "blue"; break;
            case PINK: s = "pink"; break;
            case RED: s = "red"; break;
            case ORANGE: s = "orange"; break;
        }
        return s;
    }
}
