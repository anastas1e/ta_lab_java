package com.epam.lab.exceptions.plants;

public class ColorException extends Exception {
    public ColorException() {}

    public ColorException(String message) {
        super(message);
    }
}
