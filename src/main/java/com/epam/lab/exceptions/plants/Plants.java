package com.epam.lab.exceptions.plants;

public class Plants {
    private Type type;
    private Color color;
    private int size;

    public Plants(int size, String color, String type) throws ColorException, TypeException {
        this.size = size;
        this.color = checkColor(color);
        this.type = checkType(type);
    }

    public Color checkColor(String stringColor) throws ColorException {
        Color color = null;
        try {
            color =  Color.valueOf(stringColor.toUpperCase());
        } catch (NullPointerException npe) {
            System.err.println(npe.getMessage());
        } catch (IllegalArgumentException iae) {
            throw new ColorException("This color does not exist!");
        }

        return color;
    }

    public Type checkType(String stringType) throws TypeException {
        Type type = null;
        try {
            type =  Type.valueOf(stringType.toUpperCase());
        } catch (NullPointerException npe) {
            System.err.println(npe.getMessage());
        } catch (IllegalArgumentException iae) {
            throw new TypeException("This type does not exist!");
        }
        return type;
    }

    @Override
    public String toString() {
        return "Size of plants: " + size + " Color of plants: "
                + color + " Type of plants: " + type ;
    }
}
