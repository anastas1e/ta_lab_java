package com.epam.lab.exceptions.plants;

public class Main {
    public static void main(String[] args) throws ColorException, TypeException {
        try {
            Plants plant1 = new Plants(5, "yellow", "ALGAE");
            System.out.println(plant1);
            Plants plant2 = new Plants(4, "Green", "algae");
            System.out.println(plant2);
            Plants plant3 = new Plants(3, "Blue", "MOSS");
            System.out.println(plant3);
            Plants plant4 = new Plants(2, "Orange", "Algae");
            System.out.println(plant4);
            Plants plant5 = new Plants(1, "violet", "AL");
            System.out.println(plant5);
        } catch (ColorException | TypeException e) {
            System.err.println(e.getMessage());
        }
    }
}
