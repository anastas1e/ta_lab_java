package com.epam.lab.exceptions.plants;

public class TypeException extends Exception {
    public TypeException() {}

    public TypeException(String message) {
        super(message);
    }
}
