package com.epam.lab.strings.bigtask.structure;

import java.util.Objects;

public class Word implements Comparable<Word> {
    private String word;

    public Word(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String toString() {
        return "Word:'" + word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Word newWord = (Word) o;
        return Objects.equals(word, newWord.word);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(word);
    }

    @Override
    public int compareTo(Word o) {
        return this.getWord().compareTo(o.getWord());
    }
}
