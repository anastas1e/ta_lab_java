package com.epam.lab.strings.bigtask;

import com.epam.lab.strings.bigtask.controller.Controller;
import com.epam.lab.strings.bigtask.structure.Sentence;
import com.epam.lab.strings.bigtask.structure.Word;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Main {
    private static Controller controller;
    public static void main(String[] args) throws IOException {
        controller = new Controller();
        List<Sentence> sentences = new ArrayList<>(controller.getSentences());
        System.out.println("\nThere are all the sentences of the text: ");
        for (Sentence s : sentences) {
            System.out.println(s.getValue());
        }
        //solve
        List<Word> uniqueWords = controller.getUniqueWords();
        System.out.println("\nHere you can see unique words of the first sentence: ");
        System.out.println(uniqueWords.toString());
        List<Sentence> sortedList = controller.getSortedListOfSentences();
        System.out.println("\nThere are all the sentences sorted by their size: ");
        for (Sentence s : sortedList) {
            System.out.println(s.getValue());
        }
        System.out.println("\nThere are words of given length in questions sentences: ");
        Set<Word> wordMap = controller.findWordsOfLengthInQuestions(3);
        for (Word w: wordMap) {
            System.out.println(w.getWord());
        }

        System.out.println("\nThere is text with deleted words starting with consonant of given length: ");
        controller.deleteWordStartsConsonant(4);
        for (Sentence s : controller.getSentences()) {
            System.out.println(s.getValue());
        }
        System.out.println("\nHere are printed words of the text by alphabet: ");
        controller.printWordsByAlphabet();

    }
}
