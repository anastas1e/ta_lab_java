package com.epam.lab.strings.bigtask.structure;

import java.util.Objects;

public class Sign implements Comparable<Sign> {
    private String sign;

    public Sign(String sign) {
        this.sign = sign;
    }
    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getSign() {
        return sign;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Sign newSign = (Sign) o;
        return Objects.equals(sign, newSign.sign);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(sign);
    }

    @Override
    public String toString() {
        return "Sign: " + sign;
    }

    @Override
    public int compareTo(Sign o) {
        return this.getSign().compareTo(o.getSign());
    }
}
