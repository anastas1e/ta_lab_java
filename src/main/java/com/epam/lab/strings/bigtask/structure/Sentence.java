package com.epam.lab.strings.bigtask.structure;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Sentence implements Comparable<Sentence>{
    private String value;
    private List<Word> words;
    private List<Sign> signs;

    public Sentence(String value) {
        this.value = value.replaceAll("\\s+", " ").trim();
        setWords();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        setWords();
    }

    private void setWords() {
        List<Word> words = new ArrayList<>();
        String[] wordsOld = value.split("\\s");
        for (String s : wordsOld) {
            words.add(new Word(s.trim()));
        }
        this.words = words;
    }

    public List<Word> getWords() {
        return words;
    }

    public void setSigns(List<Sign> signs) {
        this.signs = signs;
    }

    public List<Sign> getSigns() {
        return signs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sentence sentence = (Sentence) o;
        return Objects.equals(value, sentence.value) &&
                Objects.equals(words, sentence.words) &&
                Objects.equals(signs, sentence.signs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, words, signs);
    }

    @Override
    public String toString() {
        return "Sentence: " + value;
    }

    @Override
    public int compareTo(Sentence o) {
        if (this.getWords().size() > o.getWords().size()) {
            return 1;
        } else if (this.getWords().size() < o.getWords().size()) {
            return -1;
        }
        return 0;
    }
}
