package com.epam.lab.strings.bigtask.controller;

import com.epam.lab.strings.bigtask.functional.Logic;
import com.epam.lab.strings.bigtask.structure.Sentence;
import com.epam.lab.strings.bigtask.structure.Word;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Controller {

    private List<Sentence> sentences;
    private Logic logic;

    public Controller() throws IOException {
        logic = new Logic();
        readFile();
    }
    private void readFile() throws IOException {
        FileReader fileReader = new FileReader("src/main/resources/Java.txt");
        Scanner scanner = new Scanner(fileReader);
        sentences = new ArrayList<>();
        while (scanner.hasNextLine()) {
            String sentence = scanner.nextLine();
            if (!sentence.trim().equals("")) {
                sentences.add(new Sentence(sentence));
            }
        }
        fileReader.close();
    }
    public List<Sentence> getSortedListOfSentences() {
        return logic.getSortedListOfSentences(sentences);
    }
    public Set<Word> findWordsOfLengthInQuestions(int length) {
        return logic.findWordsOfLengthInQuestions(sentences, length);
    }
    public List<Word> getUniqueWords() {
        return (List<Word>) logic.getUniqueWords(sentences);
    }
    public void printWordsByAlphabet() {
        logic.printWordsByAlphabet(sentences);
    }
    public void deleteWordStartsConsonant(int length) {
        logic.deleteWordStartsConsonant(sentences, length);
    }
    public List<Sentence> getSentences() {
        return sentences;
    }
}
