package com.epam.lab.strings.bigtask.functional;

import com.epam.lab.strings.bigtask.structure.Sentence;
import com.epam.lab.strings.bigtask.structure.Word;

import java.util.*;
import java.util.stream.Collectors;

public class Logic {
    //2
    public List<Sentence> getSortedListOfSentences(List<Sentence> sentences) {
        List<Sentence> sortedList = new ArrayList<>(sentences);
        Collections.sort(sortedList);
        return sortedList;
    }
    //3
    public List<Word> getUniqueWords(List<Sentence> sentences) {
        boolean found = false;
        Sentence initialSentence = sentences.get(0);
        List<Word> uniqueWords = new ArrayList<>();
        for (Word word: initialSentence.getWords()) {
            for (Sentence s: sentences) {
                for (Word w: s.getWords()) {
                    if (w.equals(word)) {
                        found = true;
                        break;
                    }
                }
            }
            if (!found) {
                uniqueWords.add(word);
            }
        }
        return uniqueWords;

    }
    //6
    public void printWordsByAlphabet(List<Sentence> sentences) {
        char currentChar = 'a';
        List<Word> allWords = new ArrayList<>();
        for (Sentence s: sentences) {
            allWords.addAll(s.getWords());
        }
        allWords = allWords.stream().distinct().collect(Collectors.toList());
        Collections.sort(allWords);
        for (Word w: allWords) {
            if (w.getWord().charAt(0) != currentChar) {
                System.out.println("\n");
            }
            System.out.println(w.getWord() + " ");
            currentChar = w.getWord().charAt(0);
        }
    }
    //4
    public Set<Word> findWordsOfLengthInQuestions(List<Sentence> sentences, int length) {
        Set<Word> wordsList = new HashSet<>();
        for (Sentence s: sentences) {
            if (s.getValue().matches(".+\\?")) {
                for (Word w: s.getWords()) {
                    if (w.getWord().length() == length) {
                        wordsList.add(w);
                    }
                }
            }
        }
        return wordsList;
    }
    //12
    public void deleteWordStartsConsonant(List<Sentence> sentences, int length) {
        for (Sentence s: sentences) {
            for (Word w: new ArrayList<>(s.getWords())) {
                if ((w.getWord(). length() == length) && (w.getWord().matches("^[^aeiouyAEIOUY].*"))) {
                    s.getWords().remove(w);
                }
            }
            StringBuilder stringBuilder = new StringBuilder();
            for (Word w: s.getWords()) {
                stringBuilder.append(" ").append(w.getWord());
            }
            s.setValue(stringBuilder.toString().trim());
        }
    }
}
