package com.epam.lab.shop;

public abstract class Flower {
    protected Colour colour;
    protected float length;
    protected float price;

    public abstract float getPrice();

    public abstract float getLength();

    public abstract Colour getColour();

    public abstract String getTitle();

    public String toString() {
        return "Title: " + getTitle() + ". Colour: " + getColour() + ". Length: "
                + getLength() + ". Price: " + getPrice();
    }
}

