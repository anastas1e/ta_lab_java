package com.epam.lab.shop;

public enum Type {
    //Can also be used in Main instead of flower types classes
    //KISS principle used
    ROSE, TULIP, LILY, CROCUS, PEONY, ORCHID, DAISY;

    public String toString() {
        String s = "";
        switch(this) {
            case ROSE: s = "Rose"; break;
            case TULIP: s = "Tulip"; break;
            case LILY: s = "Lily"; break;
            case CROCUS: s = "Crocus"; break;
            case PEONY: s = "Peony"; break;
            case ORCHID: s = "Orchid"; break;
            case DAISY: s = "Daisy"; break;
            default: s = "Flowerpot"; break;
        }
        return s;
    }
}
