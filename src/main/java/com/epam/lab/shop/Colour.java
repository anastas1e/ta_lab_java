package com.epam.lab.shop;

public enum Colour {
    YELLOW, BLUE, WHITE, PINK, RED, ORANGE, VIOLET;

    //KISS principle used
    public String toString() {
        String s = "";
        switch (this) {
            case YELLOW:
                s = "yellow";
                break;
            case BLUE:
                s = "blue";
                break;
            case RED:
                s = "red";
                break;
            case ORANGE:
                s = "orange";
                break;
            case WHITE:
                s = "white";
                break;
            case PINK:
                s = "pink";
                break;
            case VIOLET:
                s = "violet";
                break;
        }
        return s;
    }
}

