package com.epam.lab.shop;

public class Main {
    public static void main(String[] args) {
        Flower tulip = new Tulip(Colour.PINK, 20, 10 );
        Flower rose = new Rose (Colour.RED, 30, 10);
        Flower orchid = new Orchid(Colour.ORANGE, 10, 10);
        Bouquet bouquet = new Bouquet();
        bouquet.addFlower(tulip);
        bouquet.addFlower(rose);
        bouquet.addFlower(orchid);
        bouquet.sortFlowers();
        System.out.println(bouquet);
    }
}












