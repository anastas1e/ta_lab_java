package com.epam.lab.shop;

import java.util.ArrayList;
import java.util.List;

public class Bouquet {
    public List<Flower> bouquet = new ArrayList<Flower>();
    public float bouquetPrice = 0;
    public void addFlower(Flower flower) {
        bouquet.add(flower);
    }

    public float getTotalPrice() {
        for (int i = 0; i < bouquet.size(); i++) {
            bouquetPrice += bouquet.get(i).getPrice();
        }
        return bouquetPrice;
    }

    public void sortFlowers() {
        bouquet.sort(new PriceSorter());
    }

    public String toString() {
        StringBuilder flowers = new StringBuilder();
        for(Flower f : bouquet){
        flowers.append(f).append("\t");
        }
        return flowers.toString();
    }
}

