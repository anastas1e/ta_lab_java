package com.epam.lab.shop;

public class Crocus extends Flower {
    public Crocus(Colour colour, float price, float length) {
        this.colour = colour;
        this.price = price;
        this.length = length;
    }
    public float getPrice() {
        return price;
    }
    public float getLength() {
        return length;
    }
    public Colour getColour() {
        return colour;
    }
    public String getTitle() {
        return "Crocus";
    }
}
