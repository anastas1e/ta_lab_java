package com.epam.lab.shop;

import java.util.Comparator;

public class PriceSorter implements Comparator<Flower> {

    public int compare(Flower flower1, Flower flower2) {
        return Float.compare(flower1.getPrice(), flower2.getPrice());
    }

}
