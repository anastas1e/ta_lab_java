package com.epam.lab.collections;

import java.util.Comparator;

public class TwoStringsComparator implements Comparator<TwoStrings> {
    @Override
    public int compare(TwoStrings o1, TwoStrings o2) {
        return o1.getSecond().compareTo(o2.getSecond());
    }
}
