package com.epam.lab.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TwoStrings implements Comparable<TwoStrings> {
    private String second;
    private String first;
    private static int index;

    public TwoStrings(String first, String second) {
        this.first = first;
        this.second = second;
    }

    public String getFirst() {
        return first;
    }

    public String getSecond() {
        return second;
    }

    @Override
    public int compareTo(TwoStrings o) {
        return first.compareTo(o.getFirst());
    }

    public static TwoStrings generate() {
        String[] countries = {"France", "Italy", "Ukraine", "UK", "USA"};
        String[] capitals = {"Paris", "Rome", "Kyiv", "London", "Washington"};
        if (index >= capitals.length) {
            index = 0;
        }
        return new TwoStrings(countries[index], capitals[index++]);
    }

    @Override
    public String toString() {
        return "TwoStrings(" +
                "first='" + first + '\'' +
                ", second='" + second + '\'' +
                ')';
    }

    public static void main(String[] args) {
        List<TwoStrings> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            list.add(TwoStrings.generate());
        }
        System.out.println("Initial generated list: ");
        System.out.println(list.toString());
        System.out.println("Sorted according to first argument: ");
        list.sort(TwoStrings::compareTo);
        System.out.println(list.toString());
        System.out.println("Sorted according to second argument: ");
        list.sort(new TwoStringsComparator());
        System.out.println(list.toString());
        System.out.println("Binary search using my Comparator: ");
        System.out.println(Collections.binarySearch(list, new TwoStrings("Denmark","Copenhagen")));
    }
}
