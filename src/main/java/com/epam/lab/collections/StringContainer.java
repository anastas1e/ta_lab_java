package com.epam.lab.collections;

import java.util.Arrays;

public class StringContainer {
    private static final int INITIAL_CAPACITY = 5;
    private String[] container;
    private int size;

    public StringContainer() {
        container = new String[INITIAL_CAPACITY];
    }

    public void add(String s) {
        if (size == container.length) {
            resize();
        }
        container[size] = s;
        size++;
    }

    public String get(int index) {
        if (index < container.length && !(index < 0)) {
            return container[index];
        }
        return null;
    }

    private void resize() {
        container = Arrays.copyOf(container, container.length + 1);
    }

    public static void main(String[] args) {
        StringContainer s = new StringContainer();
        s.add("cat");
        s.add("dog");
        s.add("parrot");
        s.add("fox");
        s.add("rabbit");
        s.add("wolf");
        s.add("mouse");
        System.out.println(s.get(1));
        System.out.println(s.get(3));
    }
}
