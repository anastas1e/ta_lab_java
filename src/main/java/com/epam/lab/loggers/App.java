package com.epam.lab.loggers;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
//Class for using will be soon :)
public class App {
    private static Logger logger = LogManager.getLogger(App.class);
    public static void main(String[] args) {
        logger.fatal("This is a fatal message");
        logger.error("This is an error message");
        logger.warn("This is a warn message");
        logger.info("This is an info message");
        logger.debug("This is a debug message");
        logger.trace("This is a trace message");
    }
}
