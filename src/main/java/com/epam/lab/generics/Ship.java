package com.epam.lab.generics;

import java.util.ArrayList;
import java.util.List;

public class Ship <T> {
    private List<T> ship = new ArrayList<>();

    public void putDroid(T droid) {
        ship.add(droid);
    }

    public T getDroid(int index) {
        return ship.get(index);
    }
}