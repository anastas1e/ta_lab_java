package com.epam.lab.generics;

public class Droid implements Comparable<Droid> {

    private int droidPower;
    private String droidName;

    public Droid(int droidPower, String droidName) {
        this.droidPower = droidPower;
        this.droidName = droidName;
    }

    public int getPower() {
        return droidPower;
    }

    @Override
    public String toString() {
        return "Name: " + droidName + "; Power: " + droidPower;
    }

    @Override
    public int compareTo(Droid o) {
        return Integer.compare(droidPower, o.getPower());
    }
}
