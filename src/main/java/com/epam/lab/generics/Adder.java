package com.epam.lab.generics;

import java.util.ArrayList;
import java.util.List;
/*
Try how you can add string into List<Integers>.
 */
public class Adder {
    public static void addToList(List list) {
       list.add("cat");
       list.add("dog");
       list.add("rabbit");
    }
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        addToList(list);
        System.out.println(list.get(0));
    }
}
