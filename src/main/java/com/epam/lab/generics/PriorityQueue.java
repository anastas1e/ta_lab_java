package com.epam.lab.generics;

import java.util.Arrays;

public class PriorityQueue<T> {
    public Object[] droids;
    private static final int DEFAULT_INITIAL_CAPACITY = 5;
    int size;

    public PriorityQueue() {
        this.droids = new Object[DEFAULT_INITIAL_CAPACITY];
    }

    private void grow(int minCapacity) {
        int oldCapacity = droids.length;
        int newCapacity = oldCapacity + ((oldCapacity < 64) ?
                                         (oldCapacity + 2) :
                                        (oldCapacity >> 1));
        droids = Arrays.copyOf(droids, newCapacity);
    }

    public void add(T droid) {
        if (droid == null) {
            throw new NullPointerException();
        }
        int i = size;
        if (i >= droids.length) {
            grow(i + 1);
        }
        siftUp(i, droid);
        size +=1;
    }

    private void siftUp(int k, T x) {
        Comparable<? super T> key = (Comparable<? super T>) x;
        while (k > 0) {
            int parent = (k - 1) >>> 1;
            Object e = droids[parent];
            if (key.compareTo((T) e) >= 0)
                break;
            droids[k] = e;
            k = parent;
        }
        droids[k] = key;
    }

    private void siftDown(int k, T x) {
        Comparable<? super T> key = (Comparable<? super T>)x;
        int half = size >>> 1;
        while (k < half) {
            int child = (k << 1) + 1;
            Object c = droids[child];
            int right = child + 1;
            if (right < size &&
                    ((Comparable<? super T>) c).compareTo((T) droids[right]) > 0)
                c = droids[child = right];
            if (key.compareTo((T) c) <= 0)
                break;
            droids[k] = c;
            k = child;
        }
        droids[k] = key;
    }

    public T removeAt(int i) {
        final Object[] res = droids;
        int s = --size;
        if (s == i) // removed last element
            res[i] = null;
        else {
            T moved = (T) res[s];
            res[s] = null;
            siftDown(i, moved);
            if (res[i] == moved) {
                siftUp(i, moved);
                if (res[i] != moved)
                    return moved;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return  Arrays.toString(droids);
    }

    public static void main(String[] args) {
        PriorityQueue<Droid> queue = new PriorityQueue<>();
        queue.add(new Droid(3,"002"));
        queue.add(new Droid(5, "004"));
        queue.add(new Droid(3, "006"));
        queue.add(new Droid(4, "001"));
        queue.add(new Droid(8, "123"));
        queue.add(new Droid(7, "167"));
        queue.add(new Droid(2, "542"));
        System.out.println(queue.toString());
        queue.removeAt(5);
        System.out.println(queue.toString());

    }
}
