package com.epam.lab.arrays;
import java.util.Arrays;

public class А {
    public static int[] tempArray(int[] a, int[] b, boolean uniqueness) {
        int[] temp = new int[a.length];
        int position = 0;
        for (int i = 0; i < a.length; i++) {
            boolean unique = true;
            for (int j = 0; j < b.length; j++) {
                if (a[i] == b[j]) {
                    unique = false;
                    break;
                }
            }
            if (uniqueness) {
                if (unique) {
                    temp[position] = a[i];
                    position++;
                }
            }
            if (!uniqueness) {
                if (!unique) {
                    temp[position] = a[i];
                    position++;
                }
            }
        }
        return temp;
    }

    public static int countNotZeros(int[] arr) {
        int counter = 0;
        for (int i=0; i< arr.length; i++) {
            if (arr[i] != 0) {
                counter++;
            }
        }
        return counter;
    }

    public static int[] findUnique(int[] a, int[] b) {
        int[] arr = tempArray(a, b, true);
        int position = countNotZeros(arr);
        int[] result = new int[position];
        for (int k = 0; k < result.length; k++) {
            result[k] = arr[k];
        }
        return result;
    }

    public static int[] findDuplicates(int[] a, int[] b) {
        int[] arr = tempArray(a, b, false);
        int position = countNotZeros(arr);
        int[] result = new int[position];
        for (int k = 0; k < result.length; k++) {
            result[k] = arr[k];
        }
        return result;
    }

    public static void main(String[] args) {
        int a[] = { 1, 2, 6, 3, 4, 5 };
        int b[] = { 1, 2, 3, 4, 7, 9};
        System.out.println(Arrays.toString(findUnique(a, b)));
        System.out.println(Arrays.toString(findDuplicates(a, b)));

    }
}