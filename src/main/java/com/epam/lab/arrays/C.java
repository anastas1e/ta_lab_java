package com.epam.lab.arrays;

import java.util.Arrays;
public class C {
    public static int countZeros(int[] arr) {
        int counter = 0;
        for (int i = 0; i<arr.length; i++) {
            if (arr[i] == 0) {
                counter++;
            }
        }
        return counter;
    }

    public static int[] removeDuplicates(int[] numbersWithDuplicates) {
        int[] result = new int[numbersWithDuplicates.length];
        int previous = numbersWithDuplicates[0];
        result[0] = previous;
        for (int i = 1; i < numbersWithDuplicates.length; i++) {
            int ch = numbersWithDuplicates[i];
            if (previous != ch) {
                result[i] = ch;
            }
            previous = ch;
        }
        int notZeros = result.length - countZeros(result);
        int[] finalArr = new int[notZeros];
        int position = 0;
        for (int i = 0; i < result.length; i++) {
            if (result[i] != 0) {
                finalArr[position] = result[i];
                position++;
            }
        }
        return finalArr;
    }

    public static void main(String[] args) {
        int[] arr = {1, 1, 1, 2, 2, 2, 6, 2, 1};
        System.out.println(Arrays.toString(removeDuplicates(arr)));
    }
}
