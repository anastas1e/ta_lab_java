package com.epam.lab.arrays;
import java.util.Arrays;

public class B {
    public static int[] deleteDuplicates(int[] arr) {
        int[] temp = new int[arr.length];
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            boolean unique = true;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) {
                    unique = false;
                    break;
               }
           }
            if (unique) {
                temp[count] = arr[i];
                count++;
            }
       }
        int[] result = new int[count];
        for (int k = 0; k < result.length; k++) {
            result[k] = temp[k];
        }
        return result;
    }

    public static void main(String[] args) {
        int[] arr = {1, 1, 2, 2, 2, 3, 4, 5, 6, 6, 6, 7};
        System.out.println(Arrays.toString(deleteDuplicates(arr)));
    }
}
