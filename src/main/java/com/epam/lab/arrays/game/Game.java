package com.epam.lab.arrays.game;

import java.util.Random;

public class Game {
    private Random random = new Random();
    private static int score = 25;
    private int deathDoors;
    private final int DOORS_COUNT = 10;

    private int[] doorsCreation() {
        int[] doors = new int[DOORS_COUNT];
        for (int i = 0; i < DOORS_COUNT; i++) {
            if (isMonster()) {
                doors[i] = monsterPower();
            } else {
                doors[i] = getPower();
            }
        }
        return doors;
    }

    private int monsterPower() {
        return -1 * (5 + random.nextInt(96));
    }

    private int getPower() {
        return 10 + random.nextInt(71);
    }

    private boolean isMonster() {
        int randomNum = random.nextInt(2);
        if (randomNum == 0) {
            return false;
        }
        return true;
    }

    private String infoDoors(int doorNum, int power) {
        String info;
        if (power < 0) {
            info = "monster";
        } else {
            info = "power";
        }
        return String.format("door %d with %s: %d power", doorNum + 1, info, power);
    }

    public void printDoors(int[] doors) {
        for (int i = 0; i < doors.length; i++) {
            System.out.println(infoDoors(i, doors[i]));
        }
    }

    public void countDeathDoors(int score, int doorNum, int[] doors) {
        if (doorNum < doors.length) {
            if (doors[doorNum] < 0 & score < Math.abs(doors[doorNum]) ) {
                deathDoors++;
                System.out.println("It was a door with a monster behind!" +
                        " You dead in " + infoDoors(doorNum, doors[doorNum]));
            } else {
                System.out.println("It was a safe door!" +
                        " You opened a " + infoDoors(doorNum, doors[doorNum]));;
            }
            doorNum++;
            countDeathDoors(score, doorNum, doors);
        }
    }

    public void printDeathDoors(int[] doors) {
        countDeathDoors(score, 0, doors);
        System.out.println("Amount of death doors: " + deathDoors);
    }

    public static void main(String[] args) {
        Game game = new Game();
        int[] doors = game.doorsCreation();
        game.printDoors(doors);
        game.printDeathDoors(doors);
    }
}
